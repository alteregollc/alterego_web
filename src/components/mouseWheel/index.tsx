import React, { useState } from "react";
import Styles from "./_.module.scss";

const Index = () => {
  const [show, setshow] = useState(true);
  setTimeout(() => {
    setshow(false);
  }, 30000);
  return (
    <div
      className={Styles.mouse_scroll}
      style={{ display: show ? "fixed" : "none" }}
      id="mouseWheel"
    >
      <div className={Styles.mouse}>
        <div className={Styles.wheel}></div>
      </div>
      <div>
        <span className={Styles.m_scroll_arrows + " " + Styles.trei}></span>
      </div>
    </div>
  );
};

export default Index;
