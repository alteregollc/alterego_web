import React, { useEffect, useState } from "react";
import { useHistory, Link } from "react-router-dom";
import "./_.scss";
import logo from "../../modules/images/logo.svg";
import pageUrl from "../../modules/pageUrl";
import MobLogo from "../../modules/images/logo.svg";
import MobMenu from "../../modules/images/menu.svg";

export const getIndex = () => {
  let index = 0;
  pageUrl.forEach((elm, i) => {
    if (elm === window.location.pathname) index = i;
  });
  return index;
};

export default function Menu() {
  const [show, setshow] = useState(false);
  let options: any;
  let history = useHistory();

  const activeChanger = (id: string) => {
    setshow(false);
    history.push("/" + id);
  };

  useEffect(() => {
    let childrens = [...options.children];
    childrens.forEach((elm) => {
      if ("/" + elm.id === window.location.pathname)
        elm.classList.add("active");
      else elm.classList.remove("active");
    });
  });

  return (
    <>
      <img
        src={MobLogo}
        alt="mobile logo"
        className={
          "menuComMob menuComMob__image menuComMob__image--logo menuComMob--show-" +
          show
        }
      />
      <img
        alt="menu logo"
        onClick={() => {
          setshow(!show);
        }}
        src={MobMenu}
        className={
          "menuComMob menuComMob__image menuComMob--menu menuComMob--show-" +
          show
        }
      />
      <div className={"menuCom menuCom--" + show}>
        <div className="menuCom__social">
          <Link to="https://facebook.com" target="_blank" title="facebook">
            <i className="fab fa-facebook-f"></i>
          </Link>
          <Link to="https://facebook.com" target="_blank" title="twitter">
            <i className="fab fa-twitter"></i>
          </Link>
          <Link to="https://facebook.com" target="_blank" title="instagram">
            <i className="fab fa-instagram"></i>
          </Link>
        </div>
        <div className="right-border"></div>
        <img src={logo} alt="Alter ego logo" className="menuCom__logo-image" />
        <div
          ref={(elm) => (options = elm)}
          className={"menuCom__options "}
          id="menuCom__options"
        >
          <p id="" className="active" onClick={() => activeChanger("")}>
            Home
          </p>
          <p id="features" onClick={() => activeChanger("features")}>
            Features
          </p>
        </div>
        <p className="bottom__paragraph">All right reserved ©</p>
      </div>
    </>
  );
}
