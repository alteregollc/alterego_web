import React from "react";
import { BrowserRouter as Router } from "react-router-dom";

//General components
import Menu from "./components/menu/menu";
import Pages from "./pages";
import MouseWheel from "./components/mouseWheel";
//Styles
import "normalize.css";
import "./modules/styles/general.scss";

function App() {
  return (
    <div className="general">
      <Router>
        <Menu />
        <Pages />
      </Router>
      <MouseWheel />
    </div>
  );
}

export default App;
