import React, { useState } from "react";
import Styles from "./_.module.scss";
import { homeData as data } from "../../../query/query";
import Slider from "react-slick";

function Index(props: any) {
  const [scrolling, setScrolling] = useState(false);
  // const [current, setcurrent] = useState(0);
  let Slide: any;
  const pageUp = () => {
    Slide.slickPrev();
  };

  const pageDown = () => {
    Slide.slickNext();
  };

  return (
    <div
      onWheel={(event: any) => {
        setScrolling(true);
        if (scrolling) {
          return;
        }
        if (event.nativeEvent.wheelDelta > 0) {
          pageUp();
        } else {
          pageDown();
        }
        setTimeout(() => {
          setScrolling(false);
        }, 500);

        let mouseWheel = document.getElementById("mouseWheel");
        if (mouseWheel) {
          mouseWheel.style.opacity = "0";
        }
      }}
      className={Styles.Container}
    >
      <Slider
        ref={(e) => {
          Slide = e;
        }}
        infinite={false}
        dots={true}
        arrows={false}
        adaptiveHeight={true}
        className={Styles.Slider}
        verticalSwiping={false}
        responsive={[
          {
            breakpoint: 850,
            settings: {
              verticalSwiping: false,
            },
          },
        ]}
        beforeChange={(currents: number, nexts: number) => {
          // setcurrent(nexts);
          if (currents === nexts) {
            if (currents === 0) {
              props.pagePrev();
            } else props.pageNext();
            // Slide.slickGoTo(0, false);
          }
        }}
      >
        {data.map((item: any, index: number) => (
          <div className={"slick-item " + Styles.Item} key={index}>
            <h1>{item.title}</h1>
            <p>{item.paragraph}</p>
          </div>
        ))}
      </Slider>
    </div>
  );
}

export default Index;
