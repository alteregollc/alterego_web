import React from "react";
import Styles from "./_.module.scss";

const Index = (props: any) => {
  return (
    <div className={Styles.person}>
      <img
        src={"/files/images/members/" + props.src}
        className={Styles.person__image}
        alt={props.firstName}
      />
      <div className={Styles.person__firstName}>
        <div className={Styles.person__firstName__centerLine}></div>
        <h1 className={Styles.person__firstName__text}>
          {props.firstName} {props.lastName}
        </h1>
        <div className={Styles.person__firstName__centerLine}></div>
      </div>
      <h2 className={Styles.person__status}>{props.status}</h2>
      {/* <div className={Styles.person__icons}>
        <a href={props.facebook} ><i className="fab fa-facebook-f"></i></a>
        <a href={props.twitter}><i className="fab fa-twitter"></i></a>
        <a href={props.instagram}><i className="fab fa-instagram"></i></a>
      </div> */}
    </div>
  );
};

export default Index;
