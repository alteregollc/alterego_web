import React, { useState } from "react";
import Styles from "./_.module.scss";
import { teamData as data } from "../../../query/query";
import Slider from "react-slick";
import Person from "./component/person";

function Index(props: any) {
  const [scrolling, setScrolling] = useState(false);

  let Slide: any;

  const pageUp = () => {
    Slide.slickPrev();
  };

  const pageDown = () => {
    Slide.slickNext();
  };

  return (
    <div
      onWheel={(event: any) => {
        setScrolling(true);
        if (scrolling) {
          return;
        }
        if (event.nativeEvent.wheelDelta > 0) {
          pageUp();
        } else {
          pageDown();
        }
        setTimeout(() => {
          setScrolling(false);
        }, 500);
        let mouseWheel = document.getElementById("mouseWheel");
        if (mouseWheel) {
          mouseWheel.style.opacity = "0";
        }
      }}
      className={"team " + Styles.Container}
    >
      <p className={Styles.Title}>MEET OUR TEAM</p>
      <Slider
        ref={(e) => {
          Slide = e;
        }}
        verticalSwiping={false}
        dots={true}
        arrows={false}
        adaptiveHeight={true}
        infinite={false}
        rows={3}
        beforeChange={(currents: number, nexts: number) => {
          if (currents === nexts) {
            if (currents === 0) {
              props.pagePrev();
            } else props.pageNext();
          }
        }}
        responsive={[
          {
            breakpoint: 850,
            settings: {
              dots: true,
              arrows: false,
              adaptiveHeight: true,
              verticalSwiping: false,
              rows: 2,
              infinite: false,
            },
          },
        ]}
        className={Styles.Slider}
      >
        {data.map((item: any, index: any) => {
          return <Person {...item} key={index} dots={true} />;
        })}
      </Slider>
    </div>
  );
}

export default Index;
