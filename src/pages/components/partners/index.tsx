import React, { useState } from "react";
import Styles from "./_.module.scss";
import { partnersData as data } from "../../../query/query";
import Slider from "react-slick";

function Index(props: any) {
  const [scrolling, setScrolling] = useState(false);
  // const [current, setcurrent] = useState(0);
  let Slide: any;
  const pageUp = () => {
    Slide.slickPrev();
  };

  const pageDown = () => {
    Slide.slickNext();
  };
  return (
    <div
      onWheel={(event: any) => {
        setScrolling(true);
        if (scrolling) {
          return;
        }
        if (event.nativeEvent.wheelDelta > 0) {
          pageUp();
        } else {
          pageDown();
        }
        setTimeout(() => {
          setScrolling(false);
        }, 500);
        let mouseWheel = document.getElementById("mouseWheel");
        if (mouseWheel) {
          mouseWheel.style.opacity = "0";
        }
      }}
      style={{ position: "relative" }}
      className={"partners " + Styles.Container}
    >
      <p className={Styles.Title}>OUR PARTNERS</p>
      <Slider
        ref={(e) => {
          Slide = e;
        }}
        dots={true}
        arrows={false}
        adaptiveHeight={true}
        infinite={false}
        rows={6}
        verticalSwiping={true}
        beforeChange={(currents: number, nexts: number) => {
          // setcurrent(nexts);
          if (currents === nexts) {
            if (currents === 0) {
              props.pagePrev();
            } else props.pageNext();
          }
        }}
        responsive={[
          {
            breakpoint: 850,
            settings: {
              dots: true,
              arrows: false,
              adaptiveHeight: true,
              rows: 4,
              verticalSwiping: false,
              infinite: false,
            },
          },
        ]}
        className={Styles.Slider}
      >
        {data.map((item: any, index: any) => {
          return (
            <div className={Styles.Partners} key={index}>
              <img src={item.src} alt={item.companyName} />
              <section>
                <p>{item.companyName}</p>
              </section>
            </div>
          );
        })}
      </Slider>
    </div>
  );
}

export default Index;
