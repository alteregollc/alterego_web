import React, { useState } from "react";
import Styles from "./_.module.scss";
import { featuresData as data } from "../../../query/query";
import Slider from "react-slick";
import { useSwipeable } from "react-swipeable";

function Index(props: any) {
  const [scrolling, setScrolling] = useState(false);
  // const [current, setcurrent] = use/State(0);
  let Slide: any;
  const pageUp = () => {
    if (
      (data.length <= 6 && window.innerWidth > 849) ||
      (data.length <= 4 && window.innerWidth < 850)
    ) {
      props.pagePrev();
    } else Slide.slickPrev();
  };

  const pageDown = () => {
    if (
      (data.length <= 6 && window.innerWidth > 849) ||
      (data.length <= 4 && window.innerWidth < 850)
    ) {
      props.pageNext();
    } else Slide.slickNext();
  };

  const handler = useSwipeable({
    onSwipedUp: () => {
      if (
        (data.length <= 6 && window.innerWidth > 849) ||
        (data.length <= 4 && window.innerWidth < 850)
      ) {
        props.pageNext();
      }
    },
    onSwipedDown: () => {
      if (
        (data.length <= 6 && window.innerWidth > 849) ||
        (data.length <= 4 && window.innerWidth < 850)
      ) {
        props.pagePrev();
        console.log("wy" + window.innerWidth);
      }
    },
  });

  return (
    <div
      {...handler}
      onWheel={(event: any) => {
        setScrolling(true);
        if (scrolling) {
          return;
        }
        if (event.nativeEvent.wheelDelta > 0) {
          pageUp();
        } else {
          pageDown();
        }
        setTimeout(() => {
          setScrolling(false);
        }, 500);
        let mouseWheel = document.getElementById("mouseWheel");
        if (mouseWheel) {
          mouseWheel.style.opacity = "0";
        }
      }}
      className={"features " + Styles.Container}
    >
      <p className={Styles.Title}>MAIN FEATURES</p>

      <Slider
        ref={(e) => {
          Slide = e;
        }}
        dots={true}
        arrows={false}
        adaptiveHeight={true}
        infinite={false}
        rows={6}
        beforeChange={(currents: number, nexts: number) => {
          // setcurrent(nexts);
          if (currents === nexts) {
            if (currents === 0) {
              props.pagePrev();
            } else props.pageNext();
          }
        }}
        responsive={[
          {
            breakpoint: 850,
            settings: {
              dots: true,
              verticalSwiping: false,
              arrows: false,
              adaptiveHeight: true,
              rows: 4,

              infinite: false,
            },
          },
        ]}
        className={Styles.Slider}
      >
        {data.map((item: any, index: any) => {
          return (
            <div className={Styles.features} key={index}>
              <img
                src={"/files/images/features/" + item.src}
                alt={item.companyName}
              />
              <section>
                <h1>{item.name}</h1>
                <p>{item.desc}</p>
              </section>
            </div>
          );
        })}
      </Slider>
    </div>
  );
}

export default Index;
