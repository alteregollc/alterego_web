import React, { useEffect } from "react";
import Home from "./components/home";
// import Partners from "./components/partners";
import Features from "./components/features";
import Styles from "./_.module.scss";
import "./_.scss";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import pageUrl from "../modules/pageUrl";
import { useHistory } from "react-router-dom";
import { getIndex } from "../components/menu/menu";

function Index() {
  const history = useHistory();

  const NextPage = () => {
    if (getIndex() < pageUrl.length - 1) {
      history.push(pageUrl[getIndex() + 1]);
    }
  };

  const PrevPage = () => {
    if (getIndex() > 0) {
      history.push(pageUrl[getIndex() - 1]);
    }
  };

  let element: any;

  useEffect(() => {
    pageUrl.forEach((elm, Index) => {
      if (window.location.pathname === elm) {
        element.style.top = Index * -100 + "vh";
      }
    });
  });

  useEffect(() => {}, [history]);

  return (
    <div ref={(e) => (element = e)} className={Styles.Body + " pages"}>
      <Home pagePrev={PrevPage} pageNext={NextPage} />
      <Features pagePrev={PrevPage} pageNext={NextPage} />
    </div>
  );
}

export default Index;
