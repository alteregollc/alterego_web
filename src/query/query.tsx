export const homeData = [
  {
    title: "ALTER EGO",
    paragraph: "Unlimited thinking/ideas. Digital culture. Experienced team",
  },
  {
    title: "ALTER EGO",
    paragraph: "Trusting. Project service. Creative-oriented",
  },
  {
    title: "ALTER EGO",
    paragraph: "Supporting Sustainable Development Goals. Excellent service. Innovative",
  },
];

export const partnersData = [
  {
    companyName: "Apple",
    src:
      "https://201758-624029-raikfcquaxqncofqfm.stackpathdns.com/wp-content/uploads/2017/05/white-apple-logo-on-black-background-md.png",
  },
  {
    companyName: "Mac",
    src:
      "https://201758-624029-raikfcquaxqncofqfm.stackpathdns.com/wp-content/uploads/2017/05/white-apple-logo-on-black-background-md.png",
  },
  {
    companyName: "Unity",
    src: "https://seeklogo.com/images/U/unity-logo-988A22E703-seeklogo.com.png",
  },
  {
    companyName: "Phython",
    src: "https://logodix.com/logo/729238.png",
  },
  {
    companyName: "React",
    src: "https://cdn.worldvectorlogo.com/logos/react.svg",
  },
  {
    companyName: "Node js",
    src: "https://seeklogo.com/images/N/nodejs-logo-FBE122E377-seeklogo.com.png",
  },
  {
    companyName: "Apple",
    src:
      "https://201758-624029-raikfcquaxqncofqfm.stackpathdns.com/wp-content/uploads/2017/05/white-apple-logo-on-black-background-md.png",
  },
  {
    companyName: "Mac",
    src:
      "https://201758-624029-raikfcquaxqncofqfm.stackpathdns.com/wp-content/uploads/2017/05/white-apple-logo-on-black-background-md.png",
  },
  {
    companyName: "Unity",
    src: "https://seeklogo.com/images/U/unity-logo-988A22E703-seeklogo.com.png",
  },
  {
    companyName: "Phython",
    src: "https://logodix.com/logo/729238.png",
  },
  {
    companyName: "React",
    src: "https://cdn.worldvectorlogo.com/logos/react.svg",
  },
  {
    companyName: "Node js",
    src: "https://seeklogo.com/images/N/nodejs-logo-FBE122E377-seeklogo.com.png",
  },
  {
    companyName: "Apple",
    src:
      "https://201758-624029-raikfcquaxqncofqfm.stackpathdns.com/wp-content/uploads/2017/05/white-apple-logo-on-black-background-md.png",
  },
  {
    companyName: "Mac",
    src:
      "https://201758-624029-raikfcquaxqncofqfm.stackpathdns.com/wp-content/uploads/2017/05/white-apple-logo-on-black-background-md.png",
  },
  {
    companyName: "Unity",
    src: "https://seeklogo.com/images/U/unity-logo-988A22E703-seeklogo.com.png",
  },
  {
    companyName: "Phython",
    src: "https://logodix.com/logo/729238.png",
  },
  {
    companyName: "React",
    src: "https://cdn.worldvectorlogo.com/logos/react.svg",
  },
  {
    companyName: "Node js",
    src: "https://seeklogo.com/images/N/nodejs-logo-FBE122E377-seeklogo.com.png",
  },
];

export const featuresData = [
  {
    src: "node.svg",
    name: "Node JS",
    desc:
      "Node.js is an open-source, cross-platform, JavaScript runtime environment that executes JavaScript code outside of a web browser.",
  },
  {
    src: "mongodb.svg",
    name: "MongoDB",
    desc: "Classified as a NoSQL database program, MongoDB uses JSON-like documents with schema.",
  },
  {
    src: "react.svg",
    name: "React JS",
    desc: "React can be used as a base in the development of single-page or mobile applications.",
  },
  {
    src: "php.svg",
    name: "PHP",
    desc:
      "PHP is a popular general-purpose scripting language that is especially suited to web development.",
  },
  {
    src: "aws.svg",
    name: "AWS",
    desc:
      "Amazon Web Services (AWS) is the world's most comprehensive and broadly adopted cloud platform.",
  },
  {
    src: "graphql.svg",
    name: "GraphQL",
    desc:
      "GraphQL provides a complete description of the data in your API, gives clients the power to ask for exactly what they need and nothing more.",
  },
  {
    src: "java.svg",
    name: "Java",
    desc:
      "Java is a general-purpose programming language that is class-based, object-oriented, and designed to have as few implementation dependencies as possible.",
  },
  {
    src: "medku.svg",
    name: "Python",
    desc:
      "Python's design philosophy emphasizes code readability with its notable use of significant whitespace.",
  },
];

export const teamData = [
  {
    firstName: "Buyantogtokh",
    lastName: "Ts.",
    src: "img6.jpg",
    status: "Co-Founder",
    facebook: "https://www.facebook.com/khb1125/",
    instagram: "https://www.instagram.com/khishigbayar34/",
    twitter: "",
  },
  {
    firstName: "Tamir",
    lastName: "Sh.",
    src: "tamir.jpg",
    status: "Mentor",
    facebook: "https://www.facebook.com/khb1125/",
    instagram: "https://www.instagram.com/khishigbayar34/",
    twitter: "",
  },
  {
    firstName: "Soyombo",
    lastName: "B.",
    src: "soyombo.jpg",
    status: "Mentor",
    facebook: "https://www.facebook.com/khb1125/",
    instagram: "https://www.instagram.com/khishigbayar34/",
    twitter: "",
  },
  {
    firstName: "Erdenetsogt",
    lastName: "A.",
    src: "erdenetsogtAvatar.png",
    status: "Senior developer",
    facebook: "https://www.facebook.com/khb1125/",
    instagram: "https://www.instagram.com/khishigbayar34/",
    twitter: "",
  },
  {
    firstName: "Mandakhbayar",
    lastName: "B.",
    src: "img1.jpg",
    status: "Project manager",
    facebook: "https://www.facebook.com/khb1125/",
    instagram: "https://www.instagram.com/khishigbayar34/",
    twitter: "",
  },
  // {
  //   firstName: "Ichinkhorloo",
  //   lastName: "Kh.",
  //   src: "ichinkhorloo.jpg",
  //   status: "Project manager",
  //   facebook: "https://www.facebook.com/khb1125/",
  //   instagram: "https://www.instagram.com/khishigbayar34/",
  //   twitter: "",
  // },
  {
    firstName: "Enkhjargal",
    lastName: "B.",
    src: "img42.jpg",
    status: "Designer",
    facebook: "https://www.facebook.com/photo/?fbid=2846065165620548&set=a.1407806356113110",
    instagram: "https://www.instagram.com/khishigbayar34/",
    twitter: "",
  },
  
  {
    firstName: "Khuselbayar",
    lastName: "M.",
    src: "img2.jpg",
    status: "Developer",
    facebook: "https://www.facebook.com/khb1125/",
    instagram: "https://www.instagram.com/khishigbayar34/",
    twitter: "",
  },
  {
    firstName: "Khishigbayar",
    lastName: "B.",
    src: "img51.jpg",
    status: "Developer",
    facebook: "https://www.facebook.com/khb1125/",
    instagram: "https://www.instagram.com/khishigbayar34/",
    twitter: "",
  },
  {
    firstName: "Sergelenchimeg",
    lastName: "E.",
    src: "img41.jpg",
    status: "Intern",
    facebook: "https://www.facebook.com/photo/?fbid=2846065165620548&set=a.1407806356113110",
    instagram: "https://www.instagram.com/khishigbayar34/",
    twitter: "",
  },
  {
    firstName: "Tilyegyen",
    lastName: "J.",
    src: "tilyegyen.jpg",
    status: "Intern",
    facebook: "https://www.facebook.com/photo/?fbid=2846065165620548&set=a.1407806356113110",
    instagram: "https://www.instagram.com/khishigbayar34/",
    twitter: "",
  },
];
